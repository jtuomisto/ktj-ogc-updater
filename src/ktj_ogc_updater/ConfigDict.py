class ConfigDict(dict):
    def __getattribute__(self, attr: str):
        if hasattr(super(), attr):
            return getattr(super(), attr)

        value = self.get(attr)

        if value is None:
            return None

        if isinstance(value, str) and len(value.strip()) == 0:
            return None

        return value
