from osgeo import ogr, osr


def equals_srs(srs_a: osr.SpatialReference, srs_b: osr.SpatialReference) -> bool:
    if srs_a is None or srs_b is None:
        return False

    return srs_a.GetAuthorityCode(None) == srs_b.GetAuthorityCode(None)


def create_srs(srs: osr.SpatialReference | str) -> osr.SpatialReference:
    if isinstance(srs, osr.SpatialReference):
        srs.AutoIdentifyEPSG()
        return srs

    spatial_ref = osr.SpatialReference()
    spatial_ref.SetFromUserInput(srs)
    spatial_ref.AutoIdentifyEPSG()

    return spatial_ref


def create_transformation(from_srs: osr.SpatialReference, to_srs: osr.SpatialReference):
    return osr.CoordinateTransformation(create_srs(from_srs), create_srs(to_srs))


def transform(geom: ogr.Geometry, trans: osr.CoordinateTransformation):
    srs = geom.GetSpatialReference()

    if srs.EPSGTreatsAsNorthingEasting():
        geom.SwapXY()

    geom.Transform(trans)
    srs = geom.GetSpatialReference()

    if srs.EPSGTreatsAsNorthingEasting():
        geom.SwapXY()
