import sys
import tomllib
from osgeo import gdal, ogr, osr

gdal.PushErrorHandler('CPLQuietErrorHandler')
gdal.UseExceptions()
ogr.UseExceptions()
osr.UseExceptions()
gdal.ConfigurePythonLogging()

from .MMLPostgres import MMLPostgres
from .ConfigDict import ConfigDict


def main():
    if len(sys.argv) != 2:
        print('Usage: ktj_ogc_updater config.toml')
        sys.exit(1)

    with open(sys.argv[1], mode='rb') as handle:
        config = ConfigDict(tomllib.load(handle))

    mml_pg = MMLPostgres()

    mml_pg.set_options(
        host=config.postgres_host,
        port=config.postgres_port,
        database=config.postgres_db,
        schema=config.postgres_schema,
        user=config.postgres_user,
        password=config.postgres_password,
        mml_user=config.mml_api_user,
        mml_password=config.mml_api_password,
        oapif_srs=config.oapif_srs,
        srs=config.postgres_srs,
        page_size=config.page_size
    )

    if config.bbox is not None and isinstance(config.bbox, list) and len(config.bbox) == 4:
        mml_pg.set_options(bbox=config.bbox)

    mml_pg.add_layer_name('PalstanSijaintitiedot', 'palstat_aineisto')
    mml_pg.add_layer_name('RajamerkinSijaintitiedot', 'rajamerkit_aineisto')
    mml_pg.add_layer_name('KiinteistorajanSijaintitiedot', 'rajat_aineisto')

    mml_pg.add_layer_attributes('palstat_aineisto', {
        'kiinteistotunnuksenEsitysmuoto': 'kiinteistotunnus'
    })
    mml_pg.add_layer_attributes('rajamerkit_aineisto', {
        'rajamerkkilaji': 'laji',
        'numero': 'numero',
        'tasosijaintitarkkuus': 'sijaintitarkkuus',
        'rakenne': 'rakenne',
        'lahdeaineisto': 'lahdeaineisto',
        'suhdeMaanpintaan': 'suhdemaanpintaan',
        'olemassaolo': 'olemassaolo'
    })
    mml_pg.add_layer_attributes('rajat_aineisto', {
        'kiinteistorajalaji': 'laji',
        'lahdeaineisto': 'lahdeaineisto'
    })

    try:
        print('Connecting to MML API and Postgres...')
        mml_pg.open()

        print('Processing features...')
        mml_pg.process_features()

        if config.sql_after_write is not None:
            print('Executing SQL...')
            mml_pg.execute_sql(config.sql_after_write)
    finally:
        mml_pg.close()
