from __future__ import annotations
from osgeo import gdal, ogr, osr
from .srs import create_srs, equals_srs, create_transformation, transform


COMMIT_N = 18000
API_URL = 'https://sopimus-paikkatieto.maanmittauslaitos.fi/kiinteisto-avoin/features/v3/'


class MMLPostgres:
    def __init__(self):
        self._dataset = None
        self._dest_dataset = None
        self._remote_srs = create_srs('EPSG:3067')
        self._result_srs = create_srs('EPSG:3067')
        self._page_size = 8000
        self._pg_host = 'localhost'
        self._pg_port = 5432
        self._pg_database = 'postgres'
        self._pg_schema = 'public'
        self._pg_user = 'postgres'
        self._pg_pass = 'postgres'
        self._mml_user = None
        self._mml_pass = None
        self._bbox_geom = None
        self._layer_name_map = {}
        self._layer_attribute_map = {}
        self._layer_attribute_map_rev = {}
        self._transformation = None

    def _create_pg_uri(self) -> str:
        return f'postgresql://{self._pg_user}:{self._pg_pass}@{self._pg_host}:{self._pg_port}/{self._pg_database}'

    def _get_ogc_layer(self, ogc_layer_name: str) -> ogr.Layer:
        layer = self._dataset.GetLayerByName(ogc_layer_name)

        if self._bbox_geom is not None:
            layer.SetSpatialFilter(self._bbox_geom)

        return layer

    def _restart_sequences(self, pg_layer_name: str):
        table = f"'{self._pg_schema}.{pg_layer_name}'"

        try:
            result = self.execute_sql(f"select pg_get_serial_sequence({table}, 'fid') as seq")

            if (feature := result.GetNextFeature()) is None:
                return

            if (name := feature.GetField('seq')) is None:
                return

            self.execute_sql(f'alter sequence {name} restart with 1')
        except RuntimeError:
            print(f'Failed to restart sequence for table {table}...')

    def _create_pg_layer(self, ogc_layer: ogr.Layer) -> ogr.Layer:
        ogc_layer_defn = ogc_layer.GetLayerDefn()
        ogc_layer_geom_type = ogc_layer.GetGeomType()
        pg_layer = None
        pg_layer_srs = ogc_layer.GetSpatialRef() if self._result_srs is None else self._result_srs

        if (pg_layer_name := self._layer_name_map.get(ogc_layer.GetName())) is None:
            return None

        pg_layer_opts = [
            'DIM=2',
            'FID=fid',
            'FID64=TRUE',
            'PRECISION=NO',
            'GEOM_TYPE=geometry',
            'LAUNDER=NO',
            'EXTRACT_SCHEMA_FROM_LAYER_NAME=OFF',
            'OVERWRITE=NO',
            'SPATIAL_INDEX=GIST',
            'GEOMETRY_NAME=geom',
            f'SCHEMA={self._pg_schema}'
        ]

        try:
            pg_layer = self._dest_dataset.CreateLayer(pg_layer_name,
                                                      pg_layer_srs,
                                                      ogc_layer_geom_type,
                                                      options=pg_layer_opts)
        except RuntimeError:
            pg_layer = self._dest_dataset.GetLayerByName(pg_layer_name)

            if pg_layer is not None:
                self._restart_sequences(pg_layer_name)
                return pg_layer

        if pg_layer is None:
            raise RuntimeError('Creating or using existing layer for postgres failed')

        for i in range(ogc_layer_defn.GetFieldCount()):
            field = ogc_layer_defn.GetFieldDefn(i)
            field_name = field.GetName()
            pg_field_name = field_name
            pg_layer_attrs = self._layer_attribute_map.get(pg_layer_name)

            if pg_layer_attrs is not None and len(pg_layer_attrs) > 0:
                if (pg_attr := pg_layer_attrs.get(field_name)) is None:
                    continue

                pg_field_name = pg_attr

            pg_field = ogr.FieldDefn(pg_field_name, field.GetType())
            pg_field.SetSubType(field.GetSubType())
            pg_layer.CreateField(pg_field)

        return pg_layer

    def _set_remote_srs(self, srs: osr.SpatialReference | str):
        if srs is None:
            return

        self._remote_srs = create_srs(srs)

        if self._result_srs is not None:
            if not equals_srs(self._remote_srs, self._result_srs):
                self._transformation = create_transformation(self._remote_srs, self._result_srs)
            else:
                self._transformation = None

        if self._bbox_geom is not None:
            self._bbox_geom.AssignSpatialReference(self._remote_srs)

    def _set_result_srs(self, srs: osr.SpatialReference | str):
        if srs is None:
            return

        self._result_srs = create_srs(srs)

        if self._remote_srs is not None:
            if not equals_srs(self._remote_srs, self._result_srs):
                self._transformation = create_transformation(self._remote_srs, self._result_srs)
            else:
                self._transformation = None

    def _set_page_size(self, page_size: int):
        if page_size is None:
            return

        self._page_size = max(10, min(10000, int(page_size)))

    def _set_town_id(self, town_id: str):
        if town_id is None:
            return

        self._town_id = str(town_id)

    def _set_bbox(self, bbox: list):
        if bbox is None:
            return

        self._bbox_geom = ogr.Geometry(ogr.wkbPolygon)
        ring = ogr.Geometry(ogr.wkbLinearRing)
        x1, y1, x2, y2 = bbox

        ring.AddPoint_2D(x1, y1)
        ring.AddPoint_2D(x1, y2)
        ring.AddPoint_2D(x2, y2)
        ring.AddPoint_2D(x2, y1)
        ring.AddPoint_2D(x1, y1)

        self._bbox_geom.AddGeometry(ring)
        self._bbox_geom.AssignSpatialReference(self._remote_srs)

    def set_options(self, **kwargs) -> MMLPostgres:
        self._pg_host = kwargs.get('host', self._pg_host)
        self._pg_port = kwargs.get('port', self._pg_port)
        self._pg_database = kwargs.get('database', self._pg_database)
        self._pg_schema = kwargs.get('schema', self._pg_schema)
        self._pg_user = kwargs.get('user', self._pg_user)
        self._pg_pass = kwargs.get('password', self._pg_pass)
        self._mml_user = kwargs.get('mml_user', self._mml_user)
        self._mml_pass = kwargs.get('mml_password', self._mml_pass)

        self._set_town_id(kwargs.get('town_id'))
        self._set_remote_srs(kwargs.get('oapif_srs'))
        self._set_result_srs(kwargs.get('srs'))
        self._set_page_size(kwargs.get('page_size'))
        self._set_bbox(kwargs.get('bbox'))

        return self

    def add_layer_name(self, ogc_layer: str, pg_layer: str) -> MMLPostgres:
        self._layer_name_map[ogc_layer] = pg_layer
        return self

    def add_layer_attributes(self, pg_layer: str, attribute_map: dict[str, str]) -> MMLPostgres:
        self._layer_attribute_map[pg_layer] = attribute_map.copy()
        self._layer_attribute_map_rev[pg_layer] = dict(zip(attribute_map.values(), attribute_map.keys()))
        return self

    def open(self):
        if self._bbox_geom is None:
            raise RuntimeError('Bounding box not set')

        gdal.SetConfigOption('OGR_TRUNCATE', 'YES')
        gdal.SetConfigOption('PG_USE_COPY', 'YES')

        ogc_opts = [f'URL={API_URL}',
                    f'USERPWD={self._mml_user}:{self._mml_pass}',
                    f'CRS={self._remote_srs}',
                    f'PAGE_SIZE={self._page_size}']
        pg_opts = [f'ACTIVE_SCHEMA={self._pg_schema}']

        self._dest_dataset = gdal.OpenEx(self._create_pg_uri(), gdal.OF_VECTOR | gdal.OF_UPDATE, open_options=pg_opts)
        self._dataset = gdal.OpenEx(f'OAPIF:{API_URL}', gdal.OF_VECTOR, open_options=ogc_opts)

    def close(self):
        if self._dataset is not None:
            self._dataset.Close()
            self._dataset = None

        if self._dest_dataset is not None:
            self._dest_dataset.Close()
            self._dest_dataset = None

    def execute_sql(self, sql: str):
        return self._dest_dataset.ExecuteSQL(sql)

    def process_features(self):
        if self._dataset is None:
            raise RuntimeError('OGC API Features connection not open')

        if self._dest_dataset is None:
            raise RuntimeError('PostgreSQL connection not open')

        counter = 0
        self._dest_dataset.StartTransaction()

        for layer_name in self._layer_name_map.keys():
            ogc_layer = self._get_ogc_layer(layer_name)
            pg_layer = self._create_pg_layer(ogc_layer)
            pg_layer_defn = pg_layer.GetLayerDefn()
            rev_attr_map = self._layer_attribute_map_rev[pg_layer.GetName()]

            while (feature := ogc_layer.GetNextFeature()) is not None:
                f = ogr.Feature(pg_layer_defn)
                f.SetGeometry(feature.GetGeometryRef())

                if self._transformation is not None:
                    transform(f.GetGeometryRef(), self._transformation)

                for i in range(pg_layer_defn.GetFieldCount()):
                    pg_field = pg_layer_defn.GetFieldDefn(i)
                    ogc_field_name = rev_attr_map[pg_field.GetName()]
                    f.SetField(i, feature.GetField(ogc_field_name))

                pg_layer.CreateFeature(f)
                counter += 1

                if counter % COMMIT_N == 0:
                    self._dest_dataset.CommitTransaction()
                    self._dest_dataset.StartTransaction()

                if counter % 10000 == 0:
                    print(f'Features written: {counter}...')

        self._dest_dataset.CommitTransaction()
        print(f'Finished with {counter} features written.')
